import math
import krpc
import time
import sqlite3
import os

# change to True if you want more console logging
# otherwise console logging is minimal
verbose = False

if not os.path.isfile('OrbitalData.db'):
    dbcon = sqlite3.connect(r"OrbitalData.db")
    print('Database created: OrbitalData')
else:
    dbcon = sqlite3.connect("OrbitalData.db")
    print('Database already exists: OrbitalData')
dbc = dbcon.cursor()
dbc.execute('PRAGMA encoding = "UTF-8";')

# connects to the kRPC server in the game client
print('Attempting to connect to KRPC server')
c = krpc.connect(name='logOrbitalData', address='127.0.0.1',
                 rpc_port=50000, stream_port=50001)
print('Connection made to KRPC server...')

# create table of celestial body orbit data if it doesn't exist
command = 'CREATE TABLE IF NOT EXISTS CelestialBodies (name text, parent text, ut float, sma float, ecc float, ' \
          'inc float, argperi float, lan float, meananomaly float, UNIQUE(name));'
dbc.execute(command)

# initial log of celestial body kepler data
cbDict = c.space_center.bodies
for name, curbody in cbDict.items():
    if not name == 'Sun':  # we don't want to take the star's data since it doesn't have any
        sma = curbody.orbit.semi_major_axis
        ecc = curbody.orbit.eccentricity
        inc = curbody.orbit.inclination * (180 / math.pi)  # need to check if this math is right for these
        argperi = curbody.orbit.argument_of_periapsis * (180 / math.pi)
        lan = curbody.orbit.longitude_of_ascending_node * (180 / math.pi)
        meananomaly = curbody.orbit.mean_anomaly
        ut = c.space_center.ut
        params = (name, curbody.orbit.body.name, ut, sma, ecc, inc, argperi, lan, meananomaly)
        command = "INSERT OR IGNORE INTO CelestialBodies VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);"
        dbc.execute(command, params)
        dbcon.commit()


def checkOrbitDelta(params):
    if processParamDelta(params['name'], 'sma', params['sma']) or processParamDelta(params['name'], 'ecc',
                                                                                    params['ecc']) or processParamDelta(
            params['name'], 'inc', params['inc']) or processParamDelta(params['name'], 'argperi',
                                                                       params['argperi']) or processParamDelta(
            params['name'], 'lan', params['lan']):
        if verbose:
            print('~~~Orbit has changed~~~')
        return True
    else:
        if verbose:
            print('~~~Orbit has not changed~~~')
        return False


def processParamDelta(name, param, value):
    # count rows, if 0 we need to add a row no matter what
    command = ("SELECT COUNT(*) FROM " + name.replace(" ", "_") + ";")
    dbc.execute(command)
    count = dbc.fetchone()[0]
    if count == 0:
        if verbose:
            print('No rows exist for ' + name + ', creating the first one.')
        return True

    # name: what table to check, param: which param to check, value: current value of that param to check against table
    command = ("SELECT " + param + " FROM " + name.replace(" ", "_") + " ORDER BY rowid DESC LIMIT 1;")
    dbc.execute(command)
    result = dbc.fetchone()[0]
    if verbose:
        print('Value: ' + str(value))
        print('Result: ' + str(result))
    percentDelta = abs(1 - (value / result))
    if verbose:
        print('Last ' + param + ' is ' + str(result) + ' & new value is ' + str(value))
        print('% Delta: ' + str(round(percentDelta, 4)))

    if percentDelta > 0.005:
        return True
    else:
        return False


def checkIfTableExists(vessel):
    if verbose:
        print('Checking if table exists for vessel named: ' + vessel)
    vessel = vessel.replace(" ", "_")
    command = 'CREATE TABLE IF NOT EXISTS ' + str(vessel) + ' (ut float, parent text, sma float, ecc float, ' \
                                                            'inc float, argperi float, lan float, meananomaly float)'
    dbc.execute(command)


def addDataToVesselTable(params):
    vessel = params['name'].replace(" ", "_")
    keplers = (params['ut'], params['parent'], params['sma'],
               params['ecc'], params['inc'],
               params['argperi'], params['lan'],
               params['meananomaly'])
    command = 'INSERT INTO ' + vessel + ' VALUES (?, ?, ?, ?, ?, ?, ?, ?)'
    dbc.execute(command, keplers)


def doesTableHaveRows(params):
    command = 'SELECT COUNT(*) FROM ' + params['name'].replace(" ", "_")
    dbc.execute(command)
    count = dbc.fetchone()[0]
    if verbose:
        print('Vessel: ' + params['name'] + ', Table rows: ' + str(count))
    if count > 0:
        return True
    else:
        return False


def tickIsBeforeLastRow(params):
    if verbose:
        print('This is the actual tickIsBeforeLastRow boolean function.')
        print('Tick UT: ' + str(params['ut']))

    command = ("SELECT ut FROM " + params['name'].replace(" ", "_") + " ORDER BY rowid DESC LIMIT 1;")
    dbc.execute(command)
    lastUT = dbc.fetchone()[0]
    if verbose:
        print('Last UT: ' + str(lastUT))
        print('Current UT: ' + str(params['ut']))
    if params['ut'] < lastUT:
        return True
    else:
        return False


def vesselHasNotLaunched(vesselstate):
    if vesselstate == 'VesselSituation.pre_launch':
        return True
    else:
        return False


def trimLastRow(params):
    command = "DELETE FROM " + params['name'].replace(" ", "_") + " WHERE rowid = (SELECT MAX(rowid) FROM " \
              + params['name'].replace(" ", "_") + ")"
    dbc.execute(command)


def revertHandler(params):
    if tickIsBeforeLastRow(params):
        if verbose:
            print(params['name'] + ': Trimming rows.')
        trimLastRow(params)
        revertHandler(params)  # recursively recall itself until there are no more table rows in the future
    else:
        if verbose:
            print(params['name'] + ': No more rows to trim.')


# the main loop
while True:
    if str(c.krpc.current_game_scene) == 'GameScene.flight':
        vesselDict = c.space_center.vessels
        print('*****')
        dt = time.time()
        if verbose:
            print('Logged system time to measure tickrate: ' + str(dt))
            print('Current UT: ' + str(c.space_center.ut))
        for vessel in vesselDict:
            try:
                # skip vessels with Debris in name and of Debris type
                if str(vessel.type) == 'VesselType.debris':
                    if verbose:
                        print(vessel.name + ': detected as Debris, ignoring logging.')
                    continue

                # first check if the table is already there.... if not, create it
                checkIfTableExists(vessel.name)
                if verbose:
                    print('** Vessel state **: ' + str(vessel.situation))
                    print('*****')
                    print('Checking if we need to write orbit data for ' + vessel.name + '...')

                # build params dict for this tick for this vessel
                # for cleaner calls and to match db columns
                params = {'name': vessel.name,
                          'ut': c.space_center.ut,
                          'parent': vessel.orbit.body.name,
                          'sma': vessel.orbit.semi_major_axis,
                          'ecc': vessel.orbit.eccentricity,
                          'inc': vessel.orbit.inclination * (180 / math.pi),  # need to check math on these three VVV
                          'argperi': vessel.orbit.argument_of_periapsis * (180 / math.pi),
                          'lan': vessel.orbit.longitude_of_ascending_node * (180 / math.pi),
                          'meananomaly': vessel.orbit.mean_anomaly}

                # this logic here protects KCT users with reverts/simluations happening
                # since after you simulate a vessel (which would log data normally), you
                # have to wait for the vessel to build, which therefore puts the UT at launch
                # after the sim data, which the simulation data needs to be wiped out
                # We check vessel state = pre_launch then wipe the table if that is the case
                if vesselHasNotLaunched(str(vessel.situation)):
                    print(params['name'] + ': Sim data detected. Emptying table data.')
                    command = 'DELETE FROM ' + params['name'].replace(" ", "_") + ';'
                    if verbose:
                        print('SQL command: ' + command)
                    dbc.execute(command)

                # make sure the table has a row to check against
                if doesTableHaveRows(params):
                    if verbose:
                        print('Rows exist in the table. No need to add a new one here.')
                else:
                    addDataToVesselTable(params)

                # handle if there is a revert
                revertHandler(params)

                # if the current tick's UT is chronologically after the last row in this vessel's table
                # pass params over to check if the orbit has changed since last tick
                if checkOrbitDelta(params):
                    addDataToVesselTable(params)
            except:
                print('Error detected, skipping logging this vessel for this loop iteration.')

        print('Tick dT: ' + str(round((1000 * (time.time() - dt)), 4)) + 'ms')
        print('*****')
    else:
        print('Not in flight scene. Running no logic.')
    time.sleep(5)
